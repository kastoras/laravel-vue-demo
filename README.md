## Laravel Vue.js Demo Application
This is a demo Laravel & Vue.js application

## Instalation
    1. Clone Repository into your server
    2. Create empty DB and User
    3. Rename .env.example to .env, or copy .env.example and change name to .env
    4. Open .env and change db setings with your DB and User
    5. Open terminal and run:
        1. composer install
        2. php artisan key:generate
        3. php artisan migrate
        4. php artisan db:seed
        5. npm install
Ready, now you can access the application, if you are working localhost here: http://localhost/laravel-vue-demo/public   

## Working Demo
You can find a working demo of this application: 
http://laravel-vue-demo.nanth.gr

## Workflow Diagram
![Workflow Diagram](documentation/application_workflow.jpg)

## TODO List
    1. Fix bug: Error when tring to save details second time for one person
    2. Create tests
    3. Comments on code