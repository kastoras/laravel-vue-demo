<?php

namespace Tests\Feature;

use App\Person;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class PersonsTest extends TestCase
{
    use DatabaseMigrations;

    public function testLoadRandomPerson(){
        $response = $this->get('persons.show', ['id'=>0])
            ->assertStatus(200);
    }

    public function testUpdatePerson(){
        
        $person = factory(Person::class)->create();

        $data = [
            'person' => [
                'first_name'=>$person->first_name,
                'last_name'=>$person->last_name,
                'email'=>$person->email
            ]
        ];

        $this->json('PUT', '/api/persons/' . $person->id, $data)
            ->assertStatus(200);
    }
}