<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'persons'; 

    public function purpose()
    {
        return $this->belongsTo('App\Purpose');
    }

    public function activities()
    {
        return $this->belongsToMany('App\Activity', 'persons_activities', 'person_id', 'activity_id');
    }
}
