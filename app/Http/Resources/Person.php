<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Purpose as PurposeResource;
use App\Http\Resources\Activities as ActivitiesResource;

class Person extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'first_name'    => $this->first_name,
            'last_name'     => $this->last_name,
            'email'         => $this->email,
            'address'       => $this->address,
            'gender'        => $this->gender,
            'purpose'       => new PurposeResource($this->purpose),
            'activities'    => ActivitiesResource::collection($this->activities),
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
        ];
    }

    public function with($request)
    {
        return  [
            'status' => 'success'
        ];
    }
}
