<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use App\Purpose;

class DetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'status'    => 'success',
            'data'      =>  [
                'activities'=>Activity::all(),
                'purposes'=>Purpose::all()
            ]
        ], 200);
    }

}
