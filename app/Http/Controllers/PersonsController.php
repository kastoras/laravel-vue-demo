<?php

namespace App\Http\Controllers;

use App\Person;
use App\Activity;
use App\Purpose;
use Illuminate\Http\Request;
use App\Http\Resources\Person as PersonResource;
use Exception;

class PersonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $person = '';
        if($id==0){
            $person = Person::all()->random();
        }
        else{
            $person = Person::find($id);
        }

        return new PersonResource($person);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $person = Person::find($id);

        $updateData = $request->all();
        foreach($updateData['person'] as $key => $personDetail){
            $person->{$key} = $personDetail;
        }

        if(isset($updateData['person_details'])){

            if(isset($updateData['person_details']['address'])){
                $person->address = $updateData['person_details']['address'];
            }
            if(isset($updateData['person_details']['gender'])){
                $person->gender = $updateData['person_details']['gender'];
            }
            if(isset($updateData['person_details']['purpose'])){
                $person->purpose_id = $updateData['person_details']['purpose'];
            }
            if(isset($updateData['person_details']['activities'])){
                $person->activities()->sync($updateData['person_details']['activities']);
            }
            
        }

        $person->save();
           
        return response()->json([
            'status' => 'success'
        ], 200);
    }

}
