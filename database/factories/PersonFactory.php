<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Person;
use Faker\Generator as Faker;

$factory->define(Person::class, function (Faker $faker) {
    return [
        'first_name'=> $faker->firstName,
        'last_name'=> $faker->lastName,
        'email'=>$faker->unique()->safeEmail,
        'address'=>$faker->streetAddress,
        'gender'=>$faker->randomElement(['Male', 'Female']),
        'purpose_id' => $faker->numberBetween(1, App\Purpose::max('id')), 
        'created_at'=> $faker->dateTimeBetween('+0 days', '+2 years'),
        'updated_at'=> $faker->dateTimeBetween('+0 days', '+2 years'),
    ];
});
