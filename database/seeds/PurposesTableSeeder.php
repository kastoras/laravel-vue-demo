<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PurposesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Purpose::insert([
            ['title' => 'business'],
            ['title' => 'pleasure'],
            ['title' => 'both']
        ]);
    }
}
