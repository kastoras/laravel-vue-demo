<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activities')->insert([
            ['title' => 'football'],
            ['title' => 'basketball'],
            ['title' => 'tennis'],
            ['title' => 'e-sports']
        ]);
    }
}
