import Vue from 'vue';
import Router from 'vue-router';
import LoadPerson from '../components/LoadPerson';
import EditDetails from '../components/EditDetails';
import Results from '../components/Results';

Vue.use(Router)

export default new Router({
    routes : [
        {
            path:'/',
            name:'load-person',
            component:LoadPerson
        },
        {
            path:'/edit-details',
            name:'EditDetails',
            component:EditDetails
        },
        {
            path:'/results',
            name:'Results',
            component:Results
        }      
    ]
})