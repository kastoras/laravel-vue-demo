import Vue from 'vue';
import Vuex from 'vuex'; 

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        person : {
            id:'',
            first_name:'',
            last_name:'',
            email:''
        },
        person_details:{
            address:'',
            gender:'',
            purpose:'',
            activities:[]
        }
    },
    getters:{
        getPersonData:state => {return state.person},
        getPersonDetails:state => {return state.person_details},
    },
    mutations: {
        setPersonBasic (state,personBasic) {
            state.person = personBasic;
        },
        setPersonDetails (state,personDetails) {
            state.person_details = personDetails;
        },    
    }
});