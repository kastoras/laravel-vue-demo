<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>window.Laravel = { csrfToken:'{{csrf_token()}}' }</script>

        <title>Laravel Vue Demo Application</title>

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="css/template/bootstrap.min.css" rel="stylesheet">
        <link href="css/template/icofont.min.css" rel="stylesheet">
        <link href="css/template/boxicons.min.css" rel="stylesheet">
        <link href="css/template/venobox.css" rel="stylesheet">


        <!-- Template Main CSS File -->
        <link href="css/template/style.css" rel="stylesheet">
        

    </head>
    <body data-aos-easing="ease">
        <div id="app">

            <!-- ======= Header ======= -->
            <navbar></navbar>
            <section id="" class="d-flex flex-column justify-content-center pt-0 pb-0">
                <div id="footer">
                    <h3><img :src="'img/vue-laravel.png'" width="100"/> Laravel Vue.js Demo Application</h3>
                    <p>Simple demo application. Brings a random person from laravel API and changes his data. App navigation at the left.</p>
                    <div class="social-links">
                    <a href="https://bitbucket.org/kastoras/laravel-vue-demo/" target="_blank" class="git"><i class='bx bxl-git'></i></i></a>
                    <a href="https://gr.linkedin.com/in/nikos-anthimidis" target="_blank" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                    </div>
                </div>
            </section><!-- End Hero -->
            <main id="main">

            <router-view/>
            <!--load-person></load-person-->

            <!--edit-details></edit-details-->

            <!--results></results-->

            </main><!-- End #main -->

            <!-- ======= Footer ======= -->
            <footer id="footer">
                <div class="container">
                    <div class="copyright">
                    &copy; Copyright <strong>Nikos</strong>. All Rights Reserved
                    </div>
                    <div class="credits">
                    <!-- All the links in the footer should remain intact. -->
                    <!-- You can delete the links only if you purchased the pro version. -->
                    <!-- Licensing information: [license-url] -->
                    <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/free-html-bootstrap-template-my-resume/ -->
                    Theme Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                    </div>
                </div>
            </footer><!-- End Footer -->

        </div>

        <script src="{{ asset('js/app.js')}}"></script>
    </body>
</html>
